﻿/*
	Пользователь вводит с клавиатуры время в секундах, прошедшее с начала дня.
	Вывести на экран текущее время в часах, минутах и секундах.
	Посчитать, сколько часов, минут и секунд осталось до полуночи.
*/

//Ваш код здесь

#include <iostream>

int main()
{
	int seconds = 0;
	int h;
	int m;
	int s;
	std::cout << "Enter time in seconds since day start: ";
	std::cin >> seconds;
	h = seconds / 3600;
	m = (seconds % 3600) / 60;
	s = (seconds % 3600) % 60;
	std::cout << "Current time is: " << h << " H " << m << " M " << s << " S\n";
	std::cout << "Till midnight: " << 24 - h << " H " << 60 - m << " M " << 60 - s << " S\n";
	return 0;
}
