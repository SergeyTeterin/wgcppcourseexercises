﻿/*
	Вычислить пройденное расстояние S при прямолинейном равноускоренном движении по формуле.
	Обозначения: v – скорость, t – время, а – ускорение.
*/

//Ваш код здесь


#include <iostream>
#include <math.h>

int main()
{
	float v = 0.0;
	float t = 0.0;
	float a = 0.0;
	std::cout << "Enter v: ";
	std::cin >> v;
	std::cout << "Enter t: ";
	std::cin >> t;
	std::cout << "Enter a: ";
	std::cin >> a;
	float result = v * t + (a * pow (t,2)) / 2;
	std::cout << "Result (S) is: " << result << ".\n";
	return 0;
}
