﻿/*
	Пользователь вводит с клавиатуры расстояние, расход бензина на 100 км и стоимость трех видов бензина.
	Вывести на экран сравнительную таблицу со стоимостью поездки на разных видах бензина.
*/

//Ваш код здесь

#include <iostream>

int main()
{
	float distance = 0;
	float consumption = 0;
	float price1 = 0;
	float price2 = 0;
	float price3 = 0;

	std::cout << "Enter distance: ";
	std::cin >> distance;
	std::cout << "Enter consumption per. 100km: ";
	std::cin >> consumption;
	std::cout << "Enter fuel 1 price: ";
	std::cin >> price1;
	std::cout << "Enter fuel 2 price: ";
	std::cin >> price2;
	std::cout << "Enter fuel 3 price: ";
	std::cin >> price3;
	std::cout << "Fuel   | " << " Total Price " << "\n";
	std::cout << "Fuel 1 | " << (distance / 100) * price1 << "\n";
	std::cout << "Fuel 2 | " << (distance / 100) * price2 << "\n";
	std::cout << "Fuel 3 | " << (distance / 100) * price3 << "\n";
	
	return 0;
}
