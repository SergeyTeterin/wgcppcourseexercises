﻿/*
	Вывести на экран один куплет любимого стихотворения или песни, с указанием автора или исполнителя.
	Используйте escape-последовательности для форматирования.
*/

//Ваш код здесь

#include <iostream>

int main()
{
	std::cout << "Mama, take this badge off of me\n";
	std::cout << "I can't use it anymore.\n";
	std::cout << "It's gettin' dark, too dark to see\n";
	std::cout << "I feel I'm knockin' on heaven's door.\n";
	std::cout << "Bob Dylan, \"Knockin' On Heaven's Door\".\n";
	return 0;
}
