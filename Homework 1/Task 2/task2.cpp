﻿/*
	Заданы три сопротивления R1, R2, R3 проводников подключённых параллельно. Вычислить значение общего сопротивления R цепи.
	Контрольный пример: R1=2, R2=4, R3=8 R = 1.142857
*/

//Ваш код здесь

#include <iostream>

int main()
{
	float r1 = 0.0;
	float r2 = 0.0;
	float r3 = 0.0;
	std::cout << "Enter R1: ";
	std::cin >> r1;
	std::cout << "Enter R2: ";
	std::cin >> r2;
	std::cout << "Enter R3: ";
	std::cin >> r3;
	float result = 1 / (1 / r1 + 1 / r2 + 1 / r3);
	std::cout << "Result is: " << result << ".\n";
	return 0;
}
